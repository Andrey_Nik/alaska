from pytest import fixture
from src.crud_bear import delete_all_bear

@fixture(scope="session", autouse=True)
def delete_all():
    delete_all_bear()
    yield
    delete_all_bear()