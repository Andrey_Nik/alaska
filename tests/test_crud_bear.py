import pytest
from src.crud_bear import get_all_bear, post_bear, get_bear


def test_get_empty_all_bear():
    response = get_all_bear()
    assert response.status_code == 200
    assert response.text == "[]"

@pytest.mark.parametrize(
        "bear_type, bear_name, bear_age",
        [
            ("BROWN", "MIKHAIL", 17.5),
            ("POLAR", "MIKHAIL", 17.5),
            ("BLACK", "MIKHAIL", 17.5),
            ("GUMMY", "MIKHAIL", 17.5)
        ]
)
def test_correct_name(bear_type, bear_name, bear_age):
    data = {"bear_type":bear_type,
            "bear_name":bear_name,
            "bear_age":bear_age}
    
    post_info = post_bear(data)
    assert post_info.status_code == 200
    data["bear_id"] = int(post_info.text)

    get_info = get_bear(post_info.text)
    assert get_info.status_code == 200
    assert get_info.json() == data


@pytest.mark.parametrize(
        "bear_type, bear_name, bear_age",
        [
            ("WHITE", "MIKHAIL", 17.5),
            ("", "MIKHAIL", 17.5),
            ("!@$", "MIKHAIL", 17.5),
            ("234", "MIKHAIL", 17.5)
        ]
)
def test_incorrect_name(bear_type, bear_name, bear_age):
    data = {"bear_type":bear_type,
            "bear_name":bear_name,
            "bear_age":bear_age}
    
    post_info = post_bear(data)
    assert post_info.status_code == 500
    


    
