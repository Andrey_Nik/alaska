install:
	python3 -m venv venv
	venv/bin/pip install -U -r requirements.txt

docker_run:
	docker run --rm -d -p 8091:8091 azshoo/alaska:1.0

test:
	PYTHONPATH=./ venv/bin/pytest -v

docker_stop:
	docker stop $$(docker ps -aq)