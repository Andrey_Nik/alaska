from requests import get, post, delete, put
from json import dumps
from settings import HOST, BEAR


def get_all_bear():
    response = get(HOST + BEAR)
    return response

def get_bear(id):
    response = get(HOST + BEAR + "/" + id)
    return response

def post_bear(data):
    response = post(HOST + BEAR, data=dumps(data))
    return response

def put_bear(data, id):
    response = put(HOST + BEAR + "/" + id, data=dumps(data))
    return response

def delete_bear(id):
    response = delete(HOST + BEAR + "/" + id)
    return response
    
def delete_all_bear():
    response = delete(HOST + BEAR)
    return response


if __name__ == "__main__":
    pass
